Phar Update
===========

[![pipeline status](https://gitlab.cylab.be/cylab/php-phar-update/badges/master/pipeline.svg)](https://gitlab.cylab.be/cylab/php-phar-update/commits/master)
[![coverage report](https://gitlab.cylab.be/cylab/php-phar-update/badges/master/coverage.svg)](https://gitlab.cylab.be/cylab/php-phar-update/commits/master)

A library for self-updating Phars.

This is a fork from abandonned project https://github.com/kherge-abandoned/php-phar-update

Summary
-------

This library handles the updating of applications packaged as distributable Phars. The modular design allows for a more customizable update process.

Installation
------------

Add it to your list of Composer dependencies:

```sh
$ composer require cylab/phar-update
```

Usage
-----

```php
<?php

use Herrera\Phar\Update\Manager;
use Herrera\Phar\Update\Manifest;

$manager = new Manager(Manifest::loadFile(
    'http://box-project.org/manifest.json'
));

// update to the next available 1.x update
$manager->update('1.0.0', true);
```
